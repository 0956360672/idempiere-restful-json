# RESTful Web Services iDempiere Plugin

- Copyright: 2019 INGEINT <https://www.ingeint.com>
- Repository: https://bitbucket.org/0956360672/idempiere-restful-json
- License: GPL 2

## Description
This is an example of how to create new endpoints for iDempiere and how to integrate RESTful definition. 

## Apache CXF
- https://cxf.apache.org/docs/jax-rs-data-bindings.html

## Features
- DELETE From Tables
- Query From Tables
- Update From Tables
- Auth process
- Restful resources definitions (enpoint, message, http status code)
- Json contract
- Exceptions handlers
- Transactions
- Version endpoint

## Instructions
- Use the [postman](https://www.getpostman.com/) collection in [docs/com.ingeint.restful.postman_collection.json](docs/com.ingeint.restful.postman_collection.json)
- Use `RequestEnv` to get the current transactions name and iDempiere context.
- Use a target platform to build the bundle.
