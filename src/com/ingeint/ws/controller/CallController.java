/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Copyright (C) 2019 INGEINT <https://www.ingeint.com> and contributors (see README.md file).
 */

package com.ingeint.ws.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.adempiere.exceptions.DBException;
import org.compiere.model.Lookup;
import org.compiere.model.MInvoice;
import org.compiere.model.MProjectPhase;
import org.compiere.model.MRole;
import org.compiere.model.MTable;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.model.POInfo;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;


import com.google.gson.Gson;
import org.apache.xerces.impl.dv.util.Base64;

@Path("/call")
@Produces(MediaType.APPLICATION_JSON)
public class CallController {


	
	


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Object post(String input) {
		
		
		   PreparedStatement pstmt = null;
			ResultSet rs = null;
			StringBuilder sql=null;
		try
		{
	   Gson gson = new Gson();
	   Map jsonObject = (Map) gson.fromJson(input, Object.class);
	   List<Map> columns=null;
	   String tablename=(String)jsonObject.get("tablename");
	   String type=(String)jsonObject.get("type");
	   List Lookupoutput=(List)jsonObject.get("Lookupoutput");
	   Map login_user=(Map)jsonObject.get("login_user");
	
		String email=(String)login_user.get("email");
	   String password=(String)login_user.get("password");
	   int roleid =Integer.parseInt((String)login_user.get("AD_Role_ID"));
   	   int client_id =Integer.parseInt((String)login_user.get("AD_Client_ID"));
	   
   	   Properties ctx = Env.getCtx();
  	   ctx.setProperty("#AD_Client_ID","1000000");
  	   ctx.setProperty("#AD_Language","en_US");
  	   ctx.setProperty("#M_Warehouse_ID","0");
   	   
   	   MRole role = new MRole(ctx, roleid, null);
   	   ctx.setProperty("#AD_Role_ID",roleid+"");
	   
	   String sql_temp="Select AD_User_ID from AD_User where EMail=?";
	   int AD_User_ID=DB.getSQLValue(null, sql_temp,email);
		
	   
		
		MUser muser=null;
		if(AD_User_ID>0)
		{
			
			
			 muser= new MUser(ctx, AD_User_ID, null);
		  
			
		     if(muser.authenticateHash(password));
				
			  else {
				  return "{\"massage\":\"Password Incorect\"}";
				  }
				  
				
		     	sql_temp="Select AD_Role_ID from AD_User_Roles where AD_Role_ID=? AND AD_User_ID=?";
				int AD_Role_ID=DB.getSQLValue(null, sql_temp,roleid,AD_User_ID);	
					
				if(AD_Role_ID>0)
				{
					sql_temp="Select AD_Client_ID from AD_Role where AD_Role_ID=? AND AD_Client_ID=?";
					int AD_Client_ID=DB.getSQLValue(null, sql_temp,AD_Role_ID,client_id);
					if(AD_Client_ID>0);
					else return "{\"massage\":\"client not allowed for this user\"}";
					
				}
				else 
					return "{\"massage\":\"Role not Allowed for This User\"}";
				
			    
		}
		else 
			return "{\"massage\":\"Email Not Found\"}";
		
		
		    sql_temp="Select AD_Table_ID from AD_Table where TableName=?";
			int tableid=DB.getSQLValue(null, sql_temp,tablename);
		
	
		if(tableid>0 || type.equals("login_user") || type.equals("query_places") );
		 else
			 return "{\"massage\":\"Tablename Not Found\"}";
	
	 POInfo poinfo = POInfo.getPOInfo(ctx, tableid);
		
	if(type.equals("query_data") )
	   {   
		 
		List columns_output=(List)jsonObject.get("columns_output");
		
		 sql=new StringBuilder("SELECT ");
		 int index=1;
		if(columns_output!=null&&columns_output.size()>0)
		 for (Object param : columns_output) {
			   
			   String name=(String)param;
			  int indx = poinfo.getColumnIndex(name);
			if (indx != -1) {		
			   sql.append(" "+name+" ");
			   
			   if(index<columns_output.size())
				   sql.append(",");
			   index++;
			   
			} else return "{\"massage\":\"out Column : "+name+" not found\"}";
			
		 }
		else sql.append(" * ");
		
		sql.append("FROM " + tablename+" WHERE AD_Client_ID = "+client_id);
			
		
		  columns=(List<Map>)jsonObject.get("columns_where");
		  if(columns.size()>0)
			   sql.append(" AND ");
		 
		   		    index=1;
		 
				   for (Map param : columns) {
					   
					   String name=(String)param.get("name");
					   String opertor=" "+(String)param.get("opertor")+" ";
					   Object value=param.get("value");
					  
					   int indx = poinfo.getColumnIndex(name);
					if (indx != -1) {		
					   sql.append(name);
					   sql.append(opertor);
					   
					   if(value instanceof List)
					   {
						StringBuilder array_in_op=new StringBuilder();
					
						array_in_op.append("(");   
						   int i=1;
						for(Object param_in:(List)param.get("value"))
						{
							 if(param_in instanceof Double && (param_in).toString().endsWith(".0"))
								 array_in_op.append("'"+(int)Math.round((Double)param_in)+"'");
							   else array_in_op.append("'"+param_in+"'");
							
							   if(i<((List)param.get("value")).size())
								   array_in_op.append(" , ");
							   i++;
						}
						array_in_op.append(")");
						value=array_in_op.toString();
					   }
					   else if(value instanceof Double && ((Double)param.get("value")).toString().endsWith(".0"))
							value="'"+(int)Math.round((Double)param.get("value"))+"'";
						else value="'"+param.get("value")+"'";
				   
					   sql.append(value);
					   
					   if(index<columns.size())
						   sql.append(" AND ");
					   index++;
				   } else return "{\"massage\":\""+name+" not fount\"}";
			}
		   
		  System.out.println(sql.toString());
		  
		
		  
		  
		  
		
		    pstmt = DB.prepareStatement(sql.toString(),null);		
		    rs = pstmt.executeQuery();
		
		    return resultSetToList(rs,poinfo,Lookupoutput);
	
	   } //endquery_data
	
	else if(type.equals("update_data"))
	{
		
		sql=new StringBuilder("SELECT "+tablename+"_ID ");
        sql.append("FROM " + tablename+" WHERE AD_Client_ID = "+client_id);
			
		
		  columns=(List<Map>)jsonObject.get("columns_where");
		  if(columns.size()>0)
			   sql.append(" AND ");
		 
		   		  int  index=1;
		 
				   for (Map param : columns) {
					   
					   String name=(String)param.get("name");
					   String opertor=" "+(String)param.get("opertor")+" ";
					   Object value=param.get("value");
					  
					   int indx = poinfo.getColumnIndex(name);
					if (indx != -1) {		
					   sql.append(name);
					   sql.append(opertor);
					   
					   if(value instanceof List)
					   {
						StringBuilder array_in_op=new StringBuilder();
					
						array_in_op.append("(");   
						   int i=1;
						for(Object param_in:(List)param.get("value"))
						{
							 if(param_in instanceof Double && (param_in).toString().endsWith(".0"))
								 array_in_op.append("'"+(int)Math.round((Double)param_in)+"'");
							   else array_in_op.append("'"+param_in+"'");
							
							   if(i<((List)param.get("value")).size())
								   array_in_op.append(" , ");
							   i++;
						}
						array_in_op.append(")");
						value=array_in_op.toString();
					   }
					   else if(value instanceof Double && ((Double)param.get("value")).toString().endsWith(".0"))
							value="'"+(int)Math.round((Double)param.get("value"))+"'";
						else value="'"+param.get("value")+"'";
				   
					   sql.append(value);
					   
					   if(index<columns.size())
						   sql.append(" AND ");
					   index++;
				   } else return "{\"massage\":\""+name+" not fount\"}";
			}
			
				   
		    pstmt = DB.prepareStatement(sql.toString(),null);		
		    rs = pstmt.executeQuery();
		   
		    List<Map<String,Object>> result_update=new ArrayList<Map<String,Object>>();
		    
		    while(rs.next())
		    {
		    	Map<String,Object> result=new HashMap<String,Object>();
		    	MTable table = MTable.get(ctx, tablename);
				PO po = table.getPO(rs.getInt(1), null);
				columns=(List<Map>)jsonObject.get("columns");
				for (Map param : columns) {
					
					String name=(String)param.get("name");
					Object value=param.get("value");
					
					int indx = poinfo.getColumnIndex(name);
					if (indx != -1) {	
					
				if(name.equals("BinaryData"))
				{	
					value=Base64.decode((String)value);
					po.set_ValueNoCheck(name,value);
				}
				
				  else { if(value instanceof Double && ((Double)param.get("value")).toString().endsWith(".0"))
							value=(int)Math.round((Double)param.get("value"));
				   else if(value instanceof Double)
		        	   value=(Double)param.get("value");
				   else if(value instanceof Timestamp)
		        	   value=(Timestamp)param.get("value");
				   else if(value instanceof Date)
		        	   value=(Date)param.get("value");
					else value=param.get("value");
					
				
				sql_temp="Select AD_Reference_ID from AD_Column where ColumnName=?";
				int AD_Reference_ID=DB.getSQLValue(null, sql_temp,(String)name);
				
				if(AD_Reference_ID==22 || AD_Reference_ID==37 || AD_Reference_ID==29 || AD_Reference_ID==12)
		 		{
		 		if(value instanceof String)
			 		 po.set_ValueNoCheck(name,new BigDecimal((String)value));
			 	else if(value instanceof Double)
			 		 po.set_ValueNoCheck(name,new BigDecimal((Double)value));
			 	else if(value instanceof Timestamp)
			        	 value=(Timestamp)param.get("value");
			       else if(value instanceof Date)
			        	 value=(Date)param.get("value");
			 		else if(value instanceof Integer)
				 		 po.set_ValueNoCheck(name,new BigDecimal((int)value));
		 		}
				else if(AD_Reference_ID==15 || AD_Reference_ID==24 || AD_Reference_ID==16)
		 		{
		 		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		 	    Date parsedDate = dateFormat.parse((String)value);
		 	    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
		 		po.set_ValueNoCheck(name,timestamp);
		 		}
				else {
		 		System.out.println("name= "+name+" value= "+value);
		 		po.set_ValueNoCheck(name,value);
		 		}
					
				
				}
					
				}
					
					else return "{\"massage\":\""+name+" not fount\"}";
				}
				if (!po.save())
					{result.put("massage", "Record Not Updated");
					result.put("RECORDID",po.get_ID());
					}
				
				else {
					
					result.put("massage", "Record Updated");
					result.put("RECORDID",po.get_ID());
					
				}
		    	
				
				result_update.add(result);
				
		    }
		
		if(result_update.size()>0)
			return result_update;
		
		else return "{\"massage\":\"No Record Found\"}";
		
		
	}
	
	else if(type.equals("create_data"))
	   {   
		MTable table = MTable.get(ctx, tablename);
		PO po = table.getPO(0, null);
		columns=(List<Map>)jsonObject.get("columns");
		for (Map param : columns) {
			
			String name=(String)param.get("name");
			Object value=param.get("value");
			
			int indx = poinfo.getColumnIndex(name);
			if (indx != -1) {	
			
		if(name.equals("BinaryData"))
		{	
			
			System.out.println("fffffffffffffffffffffffffffff");
			
			value=Base64.decode((String)value);
			po.set_ValueNoCheck(name,value);
		}
		
		else {	  if(value instanceof Double && ((Double)param.get("value")).toString().endsWith(".0"))
					value=(int)Math.round((Double)param.get("value"));
		        else if(value instanceof Double)
		        	 value=(Double)param.get("value");
		        else if(value instanceof Timestamp)
		        	 value=(Timestamp)param.get("value");
		        else if(value instanceof Date)
		        	 value=(Date)param.get("value");
				else value=param.get("value");
			
		
		
		sql_temp="Select AD_Reference_ID from AD_Column where ColumnName=?";
		int AD_Reference_ID=DB.getSQLValue(null, sql_temp,(String)name);
		
		 	if(AD_Reference_ID==22 || AD_Reference_ID==37 || AD_Reference_ID==29 || AD_Reference_ID==12)
		 		{
		 		if(value instanceof String)
			 		 po.set_ValueNoCheck(name,new BigDecimal((String)value));
			 		else if(value instanceof Double)
			 		 po.set_ValueNoCheck(name,new BigDecimal((Double)value));
			 		else if(value instanceof Integer)
				 		 po.set_ValueNoCheck(name,new BigDecimal((int)value));
		 		}
		 	else if(AD_Reference_ID==15 || AD_Reference_ID==24 || AD_Reference_ID==16)
	 		{
	 		 SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	 	    Date parsedDate = dateFormat.parse((String)value);
	 	    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
	 		po.set_ValueNoCheck(name,timestamp);
	 		}
		  	else {
		 		System.out.println("name= "+name+" value= "+value);
		 		po.set_ValueNoCheck(name,value);
		 		}
		
		}
			
		}
			
			else return "{\"massage\":\""+name+" not fount\"}";
		}
		if (!po.save())
			return "{\"massage\":\"Record Not Save\"}";
		else return "{\"RECORDID\":\""+po.get_ID()+"\"}";
		
	   }
	 
	else if(type.equals("login_user"))
	{
		Map login_column=(Map)jsonObject.get("columns");	
		if(login_column.containsKey("EMail"));
		else return "{\"massage\":\"Input EMail Not Found\"}";
		
		if(login_column.containsKey("Password"));
		else return "{\"massage\":\"Input Password Not Found\"}";
		
		    email=(String)login_column.get("EMail");
		    password=(String)login_column.get("Password");
		    sql_temp="Select AD_User_ID from AD_User where EMail=?";
		    AD_User_ID=DB.getSQLValue(null, sql_temp,email);
			
			
			
			 muser=null;
			if(AD_User_ID>0)
			{
				
				
				 muser= new MUser(ctx, AD_User_ID, null);
				  if(muser.authenticateHash(password))
				  {
					Map<String,Object> user=new HashMap<String,Object>();  
					user.put("AD_User_ID", muser.getAD_User_ID());
					user.put("EMail", muser.getEMail());
					user.put("Name", muser.getName());
					user.put("C_BPartner_ID", muser.getC_BPartner_ID());
					user.put("Value", muser.get_Value("Value"));
					
					return user;
				  }
					
				  else {
					  return "{\"massage\":\"Password login Incorect\"}";
					  }
			}
		
			else return "{\"massage\":\"EMail Not Found\"}";
		
	}
	
	else 	if(type.equals("delete_data") )
	   {   
		 
		List columns_output=(List)jsonObject.get("columns_output");
		
		 sql=new StringBuilder("DELETE ");
	     sql.append("FROM " + tablename);
			
		
		  columns=(List<Map>)jsonObject.get("columns_where");
		  if(columns.size()>0)
			   sql.append(" WHERE ");
		 
		   		   int index=1;
		 
				   for (Map param : columns) {
					   
					   String name=(String)param.get("name");
					   String opertor=" "+(String)param.get("opertor")+" ";
					   Object value=param.get("value");
					  
					   int indx = poinfo.getColumnIndex(name);
					if (indx != -1) {		
					   sql.append(name);
					   sql.append(opertor);
					   
					   if(value instanceof List)
					   {
						StringBuilder array_in_op=new StringBuilder();
					
						array_in_op.append("(");   
						   int i=1;
						for(Object param_in:(List)param.get("value"))
						{
							 if(param_in instanceof Double && (param_in).toString().endsWith(".0"))
								 array_in_op.append("'"+(int)Math.round((Double)param_in)+"'");
							   else array_in_op.append("'"+param_in+"'");
							
							   if(i<((List)param.get("value")).size())
								   array_in_op.append(" , ");
							   i++;
						}
						array_in_op.append(")");
						value=array_in_op.toString();
					   }
					   else if(value instanceof Double && ((Double)param.get("value")).toString().endsWith(".0"))
							value="'"+(int)Math.round((Double)param.get("value"))+"'";
						else value="'"+param.get("value")+"'";
				   
					   sql.append(value);
					   
					   if(index<columns.size())
						   sql.append(" AND ");
					   index++;
				   } else return "{\"massage\":\""+name+" not fount\"}";
			}
		   
		  System.out.println(sql.toString());
		  
		
		  
		  
		  
		
		    pstmt = DB.prepareStatement(sql.toString(),null);		
		 return "{\"IsExecute\":\""+!pstmt.execute()+"\"}" ;
	
	   }

	
	else return "{\"massage\":"+type+" not fount\"}";
	
	 
	  }
		catch (SQLException e)
		{
			throw new DBException(e, sql.toString());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
	   
		return null;
	 
	}

	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") int id) {
		//
	}

	
	private List<Map<String, Object>> resultSetToList(ResultSet rs,POInfo poinfo,List Lookuparray) throws SQLException {
	    
		
		
		ResultSetMetaData md = rs.getMetaData();
	    int columns = md.getColumnCount();
	    List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
	    while (rs.next()){
	        Map<String, Object> row = new HashMap<String, Object>(columns);
	        
	        for(int j = 1; j <=columns ; j++)
	          for(int i = 0; i <poinfo.getColumnCount() ; i++)
	         {
	        	
	        	String ColumnName=poinfo.getColumnName(i);
	        	
	        	if(md.getColumnName(j).equals(ColumnName.toLowerCase()))
	        	{
	        	if(Lookuparray!=null && Lookuparray.contains(ColumnName) &&
	        			poinfo.getColumnLookup(i) instanceof Lookup)
	        	{	
	        		row.put(ColumnName,poinfo.getColumnLookup(i).
	        				 getDisplay(rs.getString(ColumnName)));
	        		}
	        
	        	else if(rs.getObject(ColumnName) instanceof byte[])
	        		 row.put(ColumnName, Base64.encode((byte[])rs.getObject(ColumnName)));
	        	else
	            row.put(ColumnName, rs.getString(ColumnName));
	        	
	        	break;
	        	}
	        }
	        rows.add(row);
	    }
	    return rows;
	}
	
}